
$(document).ready(function(){

	$("#design").hide();
	$("#research").hide();
	$("#music").hide();
	$(".art").show();
	$(".design").hide();
	$(".research").hide();
	$(".music").hide();

	
	$(".dropdown").hover(function() {
		$("#design").show();
		$("#research").show();
		$("#music").show();

	}, function() {
		$("#design").hide();
		$("#research").hide();
		$("#music").hide();

	});

	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

	$("#art").click(function() {
		$(".art").show();
		$(".design").hide();
		$(".research").hide();
		$(".music").hide();
	});

	$("#design").click(function() {
		$(".art").hide();
		$(".design").show();
		$(".research").hide();
		$(".music").hide();
	});

	$("#research").click(function() {
		$(".art").hide();
		$(".design").hide();
		$(".research").show();
		$(".music").hide();
	});

		$("#music").click(function() {
		$(".art").hide();
		$(".design").hide();
		$(".research").hide();
		$(".music").show();
	});
});